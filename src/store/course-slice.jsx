/* eslint-disable no-param-reassign */
// 3rd party
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  items: [],
  currentEdit: '',
  search: {
    title: '',
    category: '',
    author: '',
  },
  sort: {
    property: '',
    increment: true,
  },
};

const courseSlice = createSlice({
  name: 'courses',
  initialState,
  reducers: {
    addCourseToList(state, action) {
      const { item } = action.payload;

      const existingItem = state.items.find(
        (element) => element.id === item.id,
      );
      if (existingItem) {
        const existingItemIndex = state.items.findIndex(
          (element) => element.id === item.id,
        );
        state.items[existingItemIndex] = item;
      } else {
        state.items.push(item);
      }
    },
    removeCourseFromList(state, action) {
      const { id } = action.payload;
      state.items = state.items.filter((element) => element.id !== id);
    },
    editCourse(state, action) {
      const { id } = action.payload;

      state.currentEdit = id;
    },
    searchCourse(state, action) {
      const { search } = action.payload;
      state.search = search;
    },
    sortHandler(state, action) {
      const { property, increment } = action.payload;
      state.sort.property = property;
      state.sort.increment = increment;
    },
  },
});

export const courseAction = courseSlice.actions;

export default courseSlice;

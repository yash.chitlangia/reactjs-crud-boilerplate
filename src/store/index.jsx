import { configureStore } from '@reduxjs/toolkit';

import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';

// slices
import courseSlice from './course-slice';
import uiSlice from './ui-slice';

const persistConfig = {
  key: 'courses',
  storage,
  whitelist: ['course'],
};

const reducer = combineReducers({
  course: courseSlice.reducer,
  ui: uiSlice.reducer,
});

const persistedReducer = persistReducer(persistConfig, reducer);

const store = configureStore({
  reducer: persistedReducer,
});

export default store;

import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  feature: 'Mock REST',
  formFieldIsValid: {
    title: false,
    category: false,
    length: false,
    author: false,
  },
};

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    switchFeatures(state, action) {
      const { feature } = action.payload;
      // eslint-disable-next-line no-param-reassign
      state.feature = feature;
    },
    toggleFormFieldValidation(state, action) {
      const data = action.payload;
      // eslint-disable-next-line no-param-reassign
      state.formFieldIsValid = { ...state.formFieldIsValid, ...data };
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice;

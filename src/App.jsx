import React from 'react';
import { Routes, Route } from 'react-router-dom';

// css
// eslint-disable-next-line no-unused-vars
import styles from './App.module.scss';

// pages
import Home from './pages/Home/Home';
import Courses from './pages/Courses/Courses';
import About from './pages/About/About';

function App() {
  // eslint-disable-next-line no-useless-return
  return (
    <div>
      <Routes>
        {/* home */}
        <Route path="/" element={<Home />} />
        {/* courses */}
        <Route path="/courses/*" element={<Courses />} />
        {/* about */}
        <Route path="/about" element={<About />} />
      </Routes>
    </div>
  );
}

export default App;

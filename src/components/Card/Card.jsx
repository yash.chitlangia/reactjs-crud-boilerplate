/* eslint-disable object-curly-newline */
/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';

// css
import styles from './Card.module.scss';

function Card({ imgURL, title, description, targetURL }) {
  return (
    <a
      href={targetURL}
      target="_blank"
      className={styles.card}
      rel="noreferrer"
    >
      <div className={styles['img-wrapper']}>
        <img src={imgURL} alt={title} />
      </div>
      <div className={styles['text-wrapper']}>
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </a>
  );
}

Card.propTypes = {
  targetURL: PropTypes.string.isRequired,
  imgURL: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
};

export default Card;

/* eslint-disable react/button-has-type */
/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';

// css
import styles from './Button.module.scss';

function Button({
  type = 'button',
  icon,
  text,
  bgColor,
  formHandler,
  disabled = false,
}) {
  return (
    <button
      disabled={disabled}
      onClick={formHandler}
      type={type}
      className={`${styles.button} ${disabled && styles.disabled}`}
      style={{ backgroundColor: bgColor }}
    >
      <span className={styles['icon-wrapper']}>{icon}</span>
      <span className={styles['text-wrapper']}>{text}</span>
    </button>
  );
}
Button.propTypes = {
  icon: PropTypes.element,
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  formHandler: PropTypes.func,
  disabled: PropTypes.bool,
};

export default Button;

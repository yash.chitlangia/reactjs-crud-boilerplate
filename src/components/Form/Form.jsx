/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable operator-linebreak */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

// 3rd party
import { v4 as uuid } from 'uuid';

// icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

// css
import styles from './Form.module.scss';

// component
import Input from './Input/Input';
import Button from '../Button/Button';

// store
import { courseAction } from '../../store/course-slice';

// initial data
const initialFormData = {
  title: '',
  category: '',
  author: '',
  length: '',
};

function Form() {
  // * store
  const courseItems = useSelector((state) => state.course.items);
  const formFieldValid = useSelector((state) => state.ui.formFieldIsValid);

  // * state
  const [formData, setFormData] = useState(initialFormData);
  const [formIsValid, setFormIsValid] = useState(false);
  const [useClearBtn, setUseClearBtn] = useState(false);

  // * icons
  const submitBtnIcon = <FontAwesomeIcon icon={faPaperPlane} />;

  // * hooks
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // * params
  const { courseId } = useParams();
  useEffect(() => {
    if (courseId) {
      const item = courseItems.find((element) => element.id === courseId);
      setFormData(item);
    }
  }, [courseId]);

  // * functions
  const inputChangeHandler = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const formSubmitHandler = (e) => {
    e.preventDefault();

    dispatch(
      courseAction.addCourseToList({
        item: { id: courseId || uuid(), ...formData },
      }),
    );
    navigate('/courses');
  };

  const clearForm = () => {
    setFormData(initialFormData);
  };
  const cancelForm = () => {
    navigate('/courses');
  };

  useEffect(() => {
    if (
      formFieldValid.title &&
      formFieldValid.category &&
      formFieldValid.length &&
      formFieldValid.author
    ) {
      setFormIsValid(true);
    }
    if (
      formFieldValid.title ||
      formFieldValid.category ||
      formFieldValid.length ||
      formFieldValid.author
    ) {
      setUseClearBtn(true);
    }
  }, [formFieldValid]);

  return (
    <div className={styles.form}>
      <h1>{courseId ? 'Edit' : 'Add'}</h1>
      <form onSubmit={formSubmitHandler}>
        <Input
          id="title"
          title="Title"
          type="text"
          placeholder="Enter title"
          name="title"
          changeHandler={inputChangeHandler}
          value={formData.title}
          validateValue={(v) => v.trim() !== ''}
        />
        <Input
          id="category"
          name="category"
          title="Category"
          type="text"
          placeholder="Enter category"
          changeHandler={inputChangeHandler}
          value={formData.category}
          validateValue={(v) => v.trim() !== ''}
        />
        <Input
          id="author"
          name="author"
          title="Author"
          type="text"
          placeholder="Enter Author"
          changeHandler={inputChangeHandler}
          value={formData.author}
          validateValue={(v) => v.trim() !== ''}
        />
        <Input
          id="length"
          name="length"
          title="Length"
          type="number"
          placeholder="Enter Length in minutes"
          changeHandler={inputChangeHandler}
          value={formData.length}
          validateValue={(v) => v > 0}
        />

        <div className={styles['form-action']}>
          <div className={styles['btn-wrapper']}>
            <Button
              type="submit"
              text="Submit"
              icon={submitBtnIcon}
              bgColor="blue"
              disabled={!formIsValid}
            />
          </div>
          <div onClick={clearForm}>
            <Button
              type="button"
              text="Clear Values"
              bgColor="grey"
              disabled={!useClearBtn}
            />
          </div>
          <div onClick={cancelForm}>
            <Button type="button" text="Cancel" bgColor="black" />
          </div>
        </div>
      </form>
    </div>
  );
}

export default Form;

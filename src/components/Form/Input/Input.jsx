/* eslint-disable react/require-default-props */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

// 3rd party
import PropTypes from 'prop-types';

// css
import styles from './Input.module.scss';

// store
import { uiActions } from '../../../store/ui-slice';

function Input({
  id,
  type,
  title,
  placeholder,
  name,
  changeHandler,
  value,
  validateValue,
}) {
  // hooks
  const dispatch = useDispatch();

  // validation
  const [isTouched, setIsTouched] = useState(false);
  const valueIsValid = validateValue(value);
  const hasError = !valueIsValid && isTouched;

  const inputChangeHandler = (e) => {
    changeHandler(e);
  };

  const inputBlurHandler = () => {
    setIsTouched(true);
  };

  useEffect(() => {
    dispatch(uiActions.toggleFormFieldValidation({ [name]: valueIsValid }));
  }, [valueIsValid]);

  return (
    <div className={styles['form-control']}>
      <label htmlFor={id}> {title} </label>
      <input
        name={name}
        id={id}
        type={type}
        placeholder={placeholder}
        onChange={inputChangeHandler}
        onBlur={inputBlurHandler}
        value={value}
      />
      {hasError && <p className={styles.error}>Field must not be empty</p>}
    </div>
  );
}

Input.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  changeHandler: PropTypes.func,
  value: PropTypes.string,
  validateValue: PropTypes.func,
};

export default Input;

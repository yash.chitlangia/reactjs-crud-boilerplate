import React from 'react';

// css

function MockRest() {
  return (
    <div>
      <h2>MockRest</h2>
      <p>Custom written mock REST</p>
    </div>
  );
}

export default MockRest;

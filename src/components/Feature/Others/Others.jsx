import React from 'react';

function Others() {
  return (
    <div>
      <h2>Others</h2>
      <ul>
        <li>Redux</li>
        <li>Redux Form</li>
        <li>Lodash</li>
        <li>React Bootstrap Table</li>
        <li>Font Awesome (for icons)</li>
        <li>Hot Module Replacement (HMR)</li>
      </ul>
    </div>
  );
}

export default Others;

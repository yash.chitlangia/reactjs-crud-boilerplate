import React from 'react';

function UnitTest() {
  return (
    <div>
      <h2>UnitTest</h2>
      <ul>
        <li>Jest</li>
        <li>Enzyme</li>
        <li>Nock</li>
        <li>Expect assertion</li>
        <li>Code coverage</li>
      </ul>
    </div>
  );
}

export default UnitTest;

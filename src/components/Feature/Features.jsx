/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// store
import { uiActions } from '../../store/ui-slice';

// css
import styles from './Features.module.scss';

// component
import MockRest from './MockRest/MockRest';
import UnitTest from './UnitTest/UnitTest';
import ES2015 from './ES2015/ES2015';
import Others from './Others/Others';

function Features() {
  // * store
  const activeFeature = useSelector((state) => state.ui.feature);
  const dispatch = useDispatch();

  // * function
  const changeTabHandler = (e) => {
    dispatch(uiActions.switchFeatures({ feature: e.target.text }));
  };

  // saves the active component in variable
  let content = <MockRest />;
  if (activeFeature === 'Mock REST') {
    content = <MockRest />;
  } else if (activeFeature === 'Unit Test') {
    content = <UnitTest />;
  } else if (activeFeature === 'ES2015') {
    content = <ES2015 />;
  } else if (activeFeature === 'Others') {
    content = <Others />;
  }

  return (
    <div className={styles.features}>
      <h1>Features</h1>

      <nav className={styles.tabs} onClick={changeTabHandler}>
        <a className={`${activeFeature === 'Mock REST' && styles.active}`}>
          Mock REST
        </a>
        <a className={`${activeFeature === 'Unit Test' && styles.active}`}>
          Unit Test
        </a>
        <a className={`${activeFeature === 'ES2015' && styles.active}`}>
          ES2015
        </a>
        <a className={`${activeFeature === 'Others' && styles.active}`}>
          Others
        </a>
      </nav>
      <div className={styles.content}>{content}</div>
    </div>
  );
}

export default Features;

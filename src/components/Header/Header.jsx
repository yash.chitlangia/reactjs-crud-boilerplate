import React from 'react';

// css
import styles from './Header.module.scss';

// component
import Nav from './Nav/Nav';

function Header() {
  return (
    <header className={styles.header}>
      <Nav />
      <div className={styles.text}>
        <h1>ReactJS CRUD Boilerplate</h1>
        <p>with mock backend</p>
      </div>
    </header>
  );
}

export default Header;

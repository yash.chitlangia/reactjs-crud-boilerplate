import React from 'react';
import { NavLink } from 'react-router-dom';

// css
import styles from './Nav.module.scss';

function Nav() {
  return (
    <nav className={styles.nav}>
      <ul>
        <NavLink
          className={(navData) => (navData.isActive ? styles.active : '')}
          to="/"
        >
          Home
        </NavLink>
        <NavLink
          className={(navData) => (navData.isActive ? styles.active : '')}
          to="/courses"
        >
          Courses
        </NavLink>
        <NavLink
          className={(navData) => (navData.isActive ? styles.active : '')}
          to="/about"
        >
          About
        </NavLink>
      </ul>
    </nav>
  );
}

export default Nav;

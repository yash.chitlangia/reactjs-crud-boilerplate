/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';

// css
import { useDispatch } from 'react-redux';
import styles from './HeaderSingle.module.scss';
import { courseAction } from '../../../store/course-slice';

function HeaderSingle({
  title,
  placeholder,
  showInput = false,
  name,
  id,
  value,
  inputChangeHandler,
}) {
  const dispatch = useDispatch();
  // const sortData = useSelector((state) => state.course.sort);

  const changeHandler = (e) => {
    inputChangeHandler(e);
  };

  const incBtnHandler = () => {
    dispatch(courseAction.sortHandler({ property: name, increment: true }));
  };
  const desBtnHandler = () => {
    dispatch(courseAction.sortHandler({ property: name, increment: false }));
  };

  return (
    <th className={styles['header-single']}>
      <div className={styles.wrapper}>
        <div className={styles['text-wrapper']}>
          <span className={styles.text}>{title}</span>
          <span className={styles.icon}>
            <FontAwesomeIcon onClick={incBtnHandler} icon={faCaretUp} />
            <FontAwesomeIcon onClick={desBtnHandler} icon={faCaretDown} />
          </span>
        </div>
        {showInput && (
          <div className={styles['input-wrapper']}>
            <input
              name={name}
              id={id}
              type="text"
              placeholder={placeholder}
              onChange={changeHandler}
              value={value}
            />
          </div>
        )}
      </div>
    </th>
  );
}

HeaderSingle.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  showInput: PropTypes.bool,
  value: PropTypes.string,
  inputChangeHandler: PropTypes.func,
};

export default HeaderSingle;

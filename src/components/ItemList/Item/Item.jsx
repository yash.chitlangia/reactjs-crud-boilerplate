/* eslint-disable object-curly-newline */
import React from 'react';
import PropTypes from 'prop-types';

// css
// eslint-disable-next-line no-unused-vars
import styles from './Item.module.scss';

function Item({ title, length, category, author }) {
  return (
    <>
      <td>{title}</td>
      <td>{length}</td>
      <td>{category}</td>
      <td>{author}</td>
    </>
  );
}

Item.propTypes = {
  title: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  category: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
};

export default Item;

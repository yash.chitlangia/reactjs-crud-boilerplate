import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// css
import styles from './ItemList.module.scss';

// component
import Item from './Item/Item';
import HeaderSingle from './HeaderSingle/HeaderSingle';

// functions
import sortFilter from '../../utils/sort';
import searchFilter from '../../utils/search-filter';

// store
import { courseAction } from '../../store/course-slice';

// initial data
const initialSearchState = {
  title: '',
  category: '',
  author: '',
};

function ItemList() {
  // * store
  const courseItem = useSelector((state) => state.course.items);
  const currentItem = useSelector((state) => state.course.currentEdit);
  const sortItem = useSelector((state) => state.course.sort);
  const searchItem = useSelector((state) => state.course.search);

  const dispatch = useDispatch();

  // * state
  const [search, setSearch] = useState(initialSearchState);

  // * functions
  const clickHandler = (e) => {
    dispatch(courseAction.editCourse({ id: e.currentTarget.id }));
  };

  const searchInputChangeHandler = (e) => {
    setSearch((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
    dispatch(
      courseAction.searchCourse({
        search: { ...search, [e.target.name]: e.target.value },
      }),
    );
  };

  useEffect(() => {
    dispatch(courseAction.searchCourse({ search }));
  }, [searchItem]);

  // filter & sort data
  let filteredData;
  filteredData = sortFilter(sortItem.property, sortItem.increment, courseItem);
  filteredData = searchFilter(searchItem, filteredData);

  return (
    <div className={styles['item-wrapper']}>
      <table>
        <thead>
          <tr className={styles['header-wrapper']}>
            <HeaderSingle
              name="title"
              id="title"
              title="Title"
              placeholder="Enter Title"
              showInput
              value={search.title}
              inputChangeHandler={searchInputChangeHandler}
            />
            <HeaderSingle
              name="length"
              id="length"
              title="Length"
              showInput={false}
            />
            <HeaderSingle
              name="category"
              id="category"
              title="Category"
              placeholder="Enter Category"
              showInput
              value={search.category}
              inputChangeHandler={searchInputChangeHandler}
            />
            <HeaderSingle
              name="author"
              id="author"
              title="Author"
              placeholder="Enter Author"
              showInput
              value={search.author}
              inputChangeHandler={searchInputChangeHandler}
            />
          </tr>
        </thead>

        <tbody>
          {filteredData.map((item) => (
            <tr
              id={item.id}
              key={item.id}
              onClick={clickHandler}
              className={`${
                currentItem === item.id && styles['active-course']
              } ${styles.course}`}
            >
              <Item
                title={item.title}
                length={item.length}
                category={item.category}
                author={item.author}
              />
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ItemList;

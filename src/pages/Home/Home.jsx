import React from 'react';

// css
import styles from './Home.module.scss';

// components
import Header from '../../components/Header/Header';
import Features from '../../components/Feature/Features';
import Card from '../../components/Card/Card';

// data
import HomeCardData from '../../data/HomeCard/HomeCardData';

function Home() {
  return (
    <div className={styles.home}>
      {/* header */}
      <Header />
      {/* main */}
      <main className={styles['card-wrapper']}>
        {HomeCardData.map((item) => (
          <Card
            key={item.id}
            targetURL={item.targetURL}
            imgURL={item.imgURL}
            title={item.title}
            description={item.description}
          />
        ))}
      </main>
      {/* features */}
      <Features />
    </div>
  );
}

export default Home;

import React from 'react';
import Nav from '../../components/Header/Nav/Nav';
// import PropTypes from 'prop-types'

// css
// eslint-disable-next-line no-unused-vars
import styles from './About.module.scss';

function About() {
  return (
    <div>
      <header>
        <Nav />
      </header>
      {/* main */}
      <main>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic
          repudiandae eligendi error earum nesciunt sed excepturi sint quaerat
          quasi! Ducimus inventore, voluptates assumenda nemo velit ipsam nobis
          qui a in.
        </p>
      </main>
    </div>
  );
}

About.propTypes = {};

export default About;

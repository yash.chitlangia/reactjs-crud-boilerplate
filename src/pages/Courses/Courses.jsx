/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable object-curly-newline */
import React from 'react';
import { useLocation, Route, Routes, useNavigate } from 'react-router-dom';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPenClip, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';

// css
import styles from './Courses.module.scss';

// component
import Nav from '../../components/Header/Nav/Nav';
import Button from '../../components/Button/Button';
import ItemList from '../../components/ItemList/ItemList';
import Form from '../../components/Form/Form';

// store
import { courseAction } from '../../store/course-slice';

// icon

function Courses() {
  // * store
  const currentEditItemId = useSelector((state) => state.course.currentEdit);

  const dispatch = useDispatch();

  // * icons
  const newIcon = <FontAwesomeIcon icon={faPlus} />;
  const editIcon = <FontAwesomeIcon icon={faPenClip} />;
  const deleteIcon = <FontAwesomeIcon icon={faTrash} />;

  const location = useLocation();
  const navigate = useNavigate();

  // * functions
  const newBtnHandler = () => {
    navigate(`${location.pathname}/new`);
  };

  const editBtnHandler = () => {
    navigate(`${location.pathname}/edit/${currentEditItemId}`);
  };

  const deleteBtnHandler = () => {
    dispatch(courseAction.removeCourseFromList({ id: currentEditItemId }));
  };

  return (
    <div className={styles.courses}>
      {/* nav */}
      <header>
        <Nav />
      </header>
      <Routes>
        <Route
          path="/"
          element={
            <div className={styles.container}>
              <h1>Courses</h1>
              <div className={styles['btn-wrapper']}>
                <Button
                  text="New"
                  icon={newIcon}
                  bgColor="#035AA5"
                  formHandler={newBtnHandler}
                />
                <Button
                  text="Edit"
                  icon={editIcon}
                  bgColor="#F0AD4E"
                  formHandler={editBtnHandler}
                />
                <Button
                  text="Deletes"
                  icon={deleteIcon}
                  bgColor="#C9382D"
                  formHandler={deleteBtnHandler}
                />
              </div>
              <div className={styles['list-wrapper']}>
                <ItemList />
              </div>
            </div>
          }
        />

        <Route path="/new" element={<Form />} />
        <Route path="/edit/:courseId" element={<Form />} />
      </Routes>
    </div>
  );
}

export default Courses;

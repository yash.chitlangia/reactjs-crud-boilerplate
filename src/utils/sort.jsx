const sortFilter = (property, increment, list) => {
  let array;
  if (property) {
    if (increment) {
      array = list.slice().sort((a, b) => (a.title > b.title ? 1 : -1));
    } else {
      array = list.slice().sort((a, b) => (a.title < b.title ? 1 : -1));
    }
    return array;
  }
  return list;
};

export default sortFilter;

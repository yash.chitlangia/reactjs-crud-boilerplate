// func accepts the array of items and
// the search keyword to filter out and return list containing partially matching titles

function search(property, searchKeyword, list) {
  const regex = new RegExp(searchKeyword, 'i');
  const filteredList = list.slice().filter((x) => regex.test(x[property]));
  return filteredList;
}
function searchFilter(searchTerms, list) {
  const titleSearch = search('title', searchTerms.title, list);
  const categorySearch = search('category', searchTerms.category, titleSearch);
  const authorSearch = search('author', searchTerms.author, categorySearch);
  return authorSearch;
}

export default searchFilter;
